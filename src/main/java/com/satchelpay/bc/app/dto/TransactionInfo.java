package com.satchelpay.bc.app.dto;

import java.util.List;

public interface TransactionInfo {

    /**
     * Unique transaction hash
     */
    String getHash();

    /**
     * Input part of transaction, can consist of either single or set of addresses
     *
     * @return addresses from which payments were delivered to endpoint
     */
    List<String> getSenders();

    /**
     * Output part of transaction, can consist of either single or set of adresses
     *
     * @return addresses to which payments were delivered from inputs
     */
    List<String> getReceivers();

    /**
     * Represent raw transaction balance
     *
     * @return balance in satoshi
     */
    long getBalanceInSatoshi();

    /**
     * Represent transaction balance in BTC equivalent
     *
     * @return balance in BTC
     */
    double getBalanceInBTC();

    /**
     * Represent raw transaction fee
     *
     * @return fee in satoshi
     */
    long getFeeInSatoshi();

    /**
     * Represent transaction fee in BTC equivalent
     *
     * @return fee in BTC
     */
    double getFeeInBTC();

    /**
     * Sign us that current transaction is first in block
     */
    boolean isCoinBase();
}
