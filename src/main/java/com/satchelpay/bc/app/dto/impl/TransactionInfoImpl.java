package com.satchelpay.bc.app.dto.impl;

import com.satchelpay.bc.app.dto.TransactionInfo;

import java.util.List;

import static info.blockchain.api.Util.SATOSHI_IN_BTC;

public class TransactionInfoImpl implements TransactionInfo {

    private long balance;
    private long fee;
    private List<String> senders;
    private List<String> receivers;
    private String hash;

    private boolean coinBaseStatus = false;

    public TransactionInfoImpl(long balance,
                               long fee,
                               List<String> senders,
                               List<String> receivers,
                               String hash) {
        this.balance = balance;
        this.fee = fee;
        this.senders = senders;
        this.receivers = receivers;
        this.hash = hash;

        if (senders.isEmpty()) {
            this.coinBaseStatus = true;
        }
    }

    @Override
    public String getHash() {
        return hash;
    }

    @Override
    public List<String> getSenders() {
        return senders;
    }

    @Override
    public List<String> getReceivers() {
        return receivers;
    }

    @Override
    public long getBalanceInSatoshi() {
        return balance;
    }

    @Override
    public double getBalanceInBTC() {
        return balance / SATOSHI_IN_BTC;
    }

    @Override
    public long getFeeInSatoshi() {
        return fee;
    }

    @Override
    public double getFeeInBTC() {
        return fee / SATOSHI_IN_BTC;
    }

    @Override
    public boolean isCoinBase() {
        return coinBaseStatus;
    }
}
