package com.satchelpay.bc.app.facade;

import com.satchelpay.bc.app.dto.TransactionInfo;
import info.blockchain.api.APIException;

import java.io.IOException;
import java.util.List;

public interface BlockchainInfoTransactionFacade {

    /**
     * Retrieve clients transactions which were safety and executed only within trusted and authorized wallet environment
     *
     * @param clientAddresses
     * @param clientAuthorizedAddresses
     * @return
     */
    List<TransactionInfo> getAuthorizedTransactionsForClientsAddresses(List<String> clientAddresses,
                                                                       List<String> clientAuthorizedAddresses) throws APIException, IOException;

    /**
     * Otherwise of authorized, return prohibited transactions which weren't executed with trusted addresses
     *
     * @param clientAddresses
     * @param clientAuthorizedAddresses
     * @return
     */
    List<TransactionInfo> getProhibitedTransactionsForClientsAddresses(List<String> clientAddresses,
                                                                       List<String> clientAuthorizedAddresses) throws APIException, IOException;

}
