package com.satchelpay.bc.app.facade.impl;

import com.satchelpay.bc.app.dto.TransactionInfo;
import com.satchelpay.bc.app.facade.BlockchainInfoTransactionFacade;
import com.satchelpay.bc.app.service.TransactionService;
import com.satchelpay.bc.app.service.impl.TransactionServiceImpl;
import info.blockchain.api.APIException;

import java.io.IOException;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class BlockchainInfoTransactionFacadeImpl implements BlockchainInfoTransactionFacade {

    private final TransactionService transactionService;

    public BlockchainInfoTransactionFacadeImpl() {
        this.transactionService = new TransactionServiceImpl();
    }

    @Override
    public List<TransactionInfo> getAuthorizedTransactionsForClientsAddresses(List<String> clientAddresses,
                                                                              List<String> clientAuthorizedAddresses) throws APIException, IOException {
        return transactionService
                .getClientsTransactions(clientAddresses)
                .stream()
                .filter(tx -> isTransactionAuthorizedForAddresses(tx, clientAddresses, clientAuthorizedAddresses))
                .collect(toList());
    }

    @Override
    public List<TransactionInfo> getProhibitedTransactionsForClientsAddresses(List<String> clientAddresses,
                                                                              List<String> clientAuthorizedAddresses) throws APIException, IOException {

        return transactionService
                .getClientsTransactions(clientAddresses)
                .stream()
                .filter(tx -> !isTransactionAuthorizedForAddresses(tx, clientAddresses, clientAuthorizedAddresses))
                .collect(toList());
    }

    private boolean isTransactionAuthorizedForAddresses(TransactionInfo tx,
                                                        List<String> clientAddresses,
                                                        List<String> clientAuthorizedAddresses) {
        if (!tx.isCoinBase()) {
            List<String> inputSenders = tx.getSenders();
            List<String> outputReceivers = tx.getReceivers();

            boolean maybeSendersAreClients = clientAddresses.containsAll(inputSenders)
                    || inputSenders.containsAll(clientAddresses);
            boolean maybeReceiversAreClientAuthorizedAddresses = clientAuthorizedAddresses.containsAll(outputReceivers)
                    || outputReceivers.containsAll(clientAuthorizedAddresses);

            boolean maybeSendersAreClientAuthorizedAddresses = clientAuthorizedAddresses.containsAll(inputSenders)
                    || inputSenders.containsAll(clientAuthorizedAddresses);
            boolean maybeReceiversAreClients = clientAddresses.containsAll(outputReceivers)
                    || outputReceivers.containsAll(clientAddresses);

                    // In case of client --> authorized address payment transfer
            return (maybeSendersAreClients && maybeReceiversAreClientAuthorizedAddresses)
                    // In case of authorized address --> client payment transfer
                    || (maybeSendersAreClientAuthorizedAddresses && maybeReceiversAreClients);
        }

        return false;
    }
}
