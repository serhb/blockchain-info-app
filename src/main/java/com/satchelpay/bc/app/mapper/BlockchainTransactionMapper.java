package com.satchelpay.bc.app.mapper;

import com.satchelpay.bc.app.dto.TransactionInfo;
import com.satchelpay.bc.app.dto.impl.TransactionInfoImpl;
import info.blockchain.api.blockexplorer.entity.Input;
import info.blockchain.api.blockexplorer.entity.Output;
import info.blockchain.api.blockexplorer.entity.Transaction;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class BlockchainTransactionMapper {

    public TransactionInfo mapToDTO(Transaction tx) {

        long balance = tx.getOutputs().stream().mapToLong(Output::getValue).sum();
        long sumThroughtInputs = tx.getInputs().stream().map(Input::getPreviousOutput).mapToLong(Output::getValue).sum();
        long fee = sumThroughtInputs - balance;
        String hash = tx.getHash();
        List<String> senders = tx.getInputs().stream().map(Input::getPreviousOutput).map(Output::getAddress).collect(toList());
        List<String> receivers = tx.getOutputs().stream().map(Output::getAddress).collect(toList());

        return new TransactionInfoImpl(balance, fee, senders, receivers, hash);
    }

}
