package com.satchelpay.bc.app.service;

import com.satchelpay.bc.app.dto.TransactionInfo;
import info.blockchain.api.APIException;

import java.io.IOException;
import java.util.List;

public interface TransactionService {

    /**
     * Retrieve all clients transactions using pagination model
     *
     * @param clientAddresses
     * @param limit
     * @param offset
     * @return
     * @throws APIException
     * @throws IOException
     */
    List<TransactionInfo> getClientsTransactions(List<String> clientAddresses, int limit, int offset) throws APIException, IOException;

    /**
     * Retrieve last 50 transactions by default
     *
     * @param clientAddresses
     * @return
     * @throws APIException
     * @throws IOException
     */
    List<TransactionInfo> getClientsTransactions(List<String> clientAddresses) throws APIException, IOException;

}
