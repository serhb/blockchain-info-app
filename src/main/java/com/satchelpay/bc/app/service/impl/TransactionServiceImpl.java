package com.satchelpay.bc.app.service.impl;

import com.satchelpay.bc.app.dto.TransactionInfo;
import com.satchelpay.bc.app.mapper.BlockchainTransactionMapper;
import com.satchelpay.bc.app.service.TransactionService;
import info.blockchain.api.APIException;
import info.blockchain.api.blockexplorer.BlockExplorer;
import info.blockchain.api.blockexplorer.entity.FilterType;

import java.io.IOException;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class TransactionServiceImpl implements TransactionService {

    private static final Integer DEFAULT_TRANSACTIONS_LIMIT = 50;

    private final BlockExplorer blockExplorer;
    private final BlockchainTransactionMapper blockchainTransactionMapper;

    public TransactionServiceImpl() {
        this.blockExplorer = new BlockExplorer();
        this.blockchainTransactionMapper = new BlockchainTransactionMapper();
    }

    public List<TransactionInfo> getClientsTransactions(List<String> clientAddresses, int limit, int offset) throws APIException, IOException {
        return blockExplorer
                .getMultiAddress(clientAddresses, FilterType.All, limit, offset)
                .getTxs()
                .stream()
                .map(blockchainTransactionMapper::mapToDTO)
                .collect(toList());
    }

    public List<TransactionInfo> getClientsTransactions(List<String> clientAddresses) throws APIException, IOException {
        return getClientsTransactions(clientAddresses, DEFAULT_TRANSACTIONS_LIMIT, 0);
    }

}
